#include "AppDelegate.h"
#include "Level.hpp"

USING_NS_CC;//using namespace cocos2d;

AppDelegate::AppDelegate()
{
	//nothing
}
///////////////////////////////////////////////////////////////////////////////

AppDelegate::~AppDelegate() 
{
	//nothing
}
///////////////////////////////////////////////////////////////////////////////

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if(!glview)
	{
		glview = GLViewImpl::create("Hello World");
		glview->setFrameSize(640, 480);
		director->setOpenGLView(glview);
    }

	// create a scene. it's an autorelease object
	auto scene = Level::createScene();

	// run
	director->runWithScene(scene);

	return true;
}
/////////////////////////////////////////////////////////////////////////////////////////

void AppDelegate::applicationDidEnterBackground()
{
	//nothing
}
//////////////////////////////////////////////////////////////////////////////////////////

void AppDelegate::applicationWillEnterForeground() 
{
	//nothing
}
