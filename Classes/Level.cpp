#include "Level.hpp"

#include"Player.hpp"
#include"Platform.hpp"

USING_NS_CC;

Scene* Level::createScene()
{
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setGravity(Vec2(0, -900));
	auto layer = Level::create();

	scene->addChild(layer);
	
	return scene;
}
//////////////////////////////////////////////////////////////////////////////

bool Level::init()
{
	if(!Layer::init())
	{
		return false;
	}
	
	player = nullptr;
	
	auto label = Label::createWithSystemFont("This is Level", "Arial", 96);
	label->setAnchorPoint(Vec2(0.5, 0.5));
	label->setPosition(Vec2(320, 240));
	
	this->addChild(label, -1);
	
	auto player = Player::create();
	assignPlayer(player);

	return true;
}
///////////////////////////////////////////////////////

void Level::addPlatform(Platform *platform)
{
	this->addChild(platform);
	platforms.push_back(platform);
}
//////////////////////////////////////////////////////////////////////////

void Level::assignPlayer(Player *player)
{
	if(this->player)
		this->removeChild(this->player);//if we had some player assinged
	if(player != nullptr)
		this->addChild(player);//in case we try to assing null player
	
	this->player = player;
}
