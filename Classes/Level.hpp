#ifndef LEVEL_HPP
#define LEVEL_HPP

#include "cocos2d.h"

#include<vector>

class Player;
class Platform;


class Level: public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(Level);
	
	
	void addPlatform(Platform *platform);
	void assignPlayer(Player *player);
	
private:

	Player *player;
	std::vector<Platform*> platforms;
};

#endif//LEVEL_HPP
